import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre: string = 'Spider-Mán';
  nombre2: string = 'cHrISthOper GUerRerO gRAndoN';
  arreglo = [1,2,3,4,5,6,7,8,9,10];
  PI: number = Math.PI;
  porcentaje: number = 0.342;
  salario: number = 1234.5;
  fecha: Date = new Date();
  idioma: string = 'es';
  videoURL: string = 'https://www.youtube.com/embed/loOWKm8GW6A';
  activar: boolean = true;

  valorPromesa = new Promise<string>((resolve , reject) => {
    setTimeout(() => {
      reject('Llego la data');
    }, 4500);
  });

  heroe = {
    nombre:'Logan',
    clave: 'Wolverin',
    edad: 500,
    direccion: {
      calle: 'Primera',
      casa: 20
    }
  }
}
